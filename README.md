# README #

### What is this repository for? ###
Playing with deep learning and reinforcement learning. At the moment it only contains a shallow network trained with DQN to solve the cart-pole problem.
Uses Keras and OpenAI gym.

### How do I get set up? ###
First, you will need Python 3. On Debian systems you can easily to this with `sudo apt-get install python3 python3-pip`

You will also need to install Numpy, Keras and OpenAI gym: `sudo pip3 install numpy keras gym`

To run the (at the moment) only script: `python3 inverted_pendulum_1.py`

### Contribution guidelines ###
I have not figured this out yet. This is just a place to share what I come up with experimenting.

### Who do I talk to? ###
Send me an email at tamassia.marco@gmail.com