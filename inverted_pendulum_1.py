import gym
import numpy as np
import random

from keras.models import Sequential
from keras.layers.core import Dense, Activation, Dropout


class RingBuffer:
    """
    A multi-field ring buffer using numpy arrays.

    Adapted from https://scimusing.wordpress.com/2013/10/25/ring-buffers-in-pythonnumpy/
    """
    def __init__(self, memory_size: int, entries_shape: tuple):
        self.index = 0
        self.size = 0
        self.data = tuple(
            np.zeros((memory_size, size), dtype=dtype)
            for size, dtype in entries_shape
        )
        self.max_size = memory_size

    def append(self, row):
        for data, new_data in zip(self.data, row):
            data[self.index, :] = new_data
        self.index = (self.index + 1) % self.max_size
        self.size = min(self.size + 1, self.max_size)

    def __get__(self, indices):
        return tuple(data[(self.index + indices) % self.max_size, :] for data in self.data)

    def get_random_entries(self, n):
        indices = np.random.randint(0, self.size, n)
        return tuple(data[indices, :] for data in self.data)


class Experience:
    """
    Experience pool, used to generate batches of random past experience.
    """
    def __init__(self, state_size: int, memory_size: int, discount: float):
        self.discount = discount
        self.memory = RingBuffer(
            memory_size=memory_size,
            entries_shape=(
                (state_size, float),
                (1, int),
                (1, float),
                (state_size, float),
                (1, bool)
            )
        )

    def remember(self, state, action, reward, new_state, game_over):
        self.memory.append((state, action, reward, new_state, game_over))

    def get_batch(self, model, batch_size):
        n_rows = min(self.memory.size, batch_size)

        S, A, R, NS, GO = self.memory.get_random_entries(n_rows)
        A, R, NGO = A.flatten(), R.flatten(), ~GO.flatten()
        inputs = S
        targets = model.predict(S)
        targets[np.arange(len(A)), A] = R
        targets[np.where(NGO), A[NGO]] += self.discount * np.max(model.predict(NS[NGO,:]),axis=1)
        return inputs, targets


def make_net(num_actions, state_size, hidden_size):
    model = Sequential()
    model.add(Dense(hidden_size, input_shape=(state_size,)))
    model.add(Activation('relu'))
    model.add(Dropout(0.2))
    model.add(Dense(num_actions))
    model.compile(optimizer="SGD", loss="MSE")
    return model


def do_the_thing():
    episodes = 1000
    batch_size = 100
    epsilon = 0.1
    epsilon_decay = 1e-03
    hidden_size = 1000
    experience_pool_size = 3000
    discount = 0.99

    env = gym.make("CartPole-v0")
    exp = Experience(env.state.size, experience_pool_size, discount)
    model = make_net(
        num_actions=env.action_space.n,
        state_size=env.state.size,
        hidden_size=hidden_size
    )

    for ep_n in range(episodes):
        state = env.reset()
        game_over = False
        loss, ret, steps = 0.0, 0, 0
        ret = 0
        while not game_over:
            env.render()

            # Choose an epsilon-greedy action
            if random.random() <= epsilon:
                action = env.action_space.sample()
            else:
                # print(model.predict(state[np.newaxis,:])[0])
                action = np.argmax(model.predict(state[np.newaxis,:])[0])
            epsilon *= (1 - epsilon_decay)

            # Collect experience
            new_state, reward, game_over, info = env.step(action)
            reward = -1 if game_over else 1 - abs(new_state[2])*10
            exp.remember(state, action, reward, new_state, game_over)
            state = new_state

            # Train model and update stats
            inputs, targets = exp.get_batch(model, batch_size=batch_size)
            loss += model.train_on_batch(inputs, targets)
            ret += reward
            steps += 1
        print("Episode {:03d}/{:03d} | Loss {:.3f} | Return {:.3f} | Steps {:d}".format(ep_n+1, episodes, loss, ret, steps))

if __name__ == '__main__':
    do_the_thing()